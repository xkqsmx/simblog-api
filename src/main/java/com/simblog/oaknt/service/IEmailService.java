package com.simblog.oaknt.service;

/**
* Created by wjj on 2016年10月13日 
 */
public interface IEmailService {

	boolean sendHtmlEmail(String title, String content, String receiveUser);
	
}
