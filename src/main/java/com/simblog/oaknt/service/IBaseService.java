package com.simblog.oaknt.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;

public interface IBaseService<T> {
	
   public int deleteByPrimaryKey(Long id);

    public int insert(T record);

    public int insertSelective(T record);

    public T selectByPrimaryKey(Long id);

    public int updateByPrimaryKeySelective(T record);

    public int updateByPrimaryKey(T record);
    
    public PageInfo<T> queryPage(Map<String, Object> params, PageInfo<T> pageInfo);
    
    public List<T> queryList(Map<String, Object> params);
    
    public Long insertReId(T id);
    
}
