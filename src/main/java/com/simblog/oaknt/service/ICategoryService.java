package com.simblog.oaknt.service;

import com.simblog.oaknt.domain.Category;
import com.simblog.oaknt.service.IBaseService;

/**
 * 
 * <br>
 * <b>功能：</b>ICategoryService<br>
 * <b>作者：</b>kezhi<br>
 * <b>日期：</b> Tue Oct 25 22:17:20 CST 2016 <br>
 * <b>版权所有： 2015,dd5s.com<br>
 */ 

public interface ICategoryService extends IBaseService<Category>{

}
