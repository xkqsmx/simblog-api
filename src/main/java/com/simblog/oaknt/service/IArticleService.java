package com.simblog.oaknt.service;

import java.util.List;
import java.util.Map;

import com.simblog.oaknt.domain.Article;
import com.simblog.oaknt.service.IBaseService;


public interface IArticleService extends IBaseService<Article>{
	public List<Map<String,Object>> countByTime(Map<String,Object> params);
	public List<Map<String,Object>> countByCategory(Map<String,Object> params);
	public List<Article> queryDetail(Map<String, Object> params);
}
