package com.simblog.oaknt.service;

import com.simblog.oaknt.domain.DataDict;
import com.simblog.oaknt.service.IBaseService;

/**
 * 
 * <br>
 * <b>功能：</b>IDataDictService<br>
 * <b>作者：</b>kezhi<br>
 * <b>日期：</b> Sun Oct 16 19:41:00 CST 2016 <br>
 * <b>版权所有： 2015,dd5s.com<br>
 */ 

public interface IDataDictService extends IBaseService<DataDict>{

}
