package com.simblog.oaknt.service;

import com.simblog.oaknt.domain.UserToken;
import com.simblog.oaknt.service.IBaseService;

/**
 * 
 * <br>
 * <b>功能：</b>IUserTokenService<br>
 * <b>作者：</b>kezhi<br>
 * <b>日期：</b> Mon Dec 19 20:49:17 CST 2016 <br>
 * <b>版权所有： 2015,dd5s.com<br>
 */ 

public interface IUserTokenService extends IBaseService<UserToken>{

}
