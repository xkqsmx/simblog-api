package com.simblog.oaknt.service;

import com.simblog.oaknt.domain.AreaDict;
import com.simblog.oaknt.service.IBaseService;

/**
 * 
 * <br>
 * <b>功能：</b>IAreaDictService<br>
 * <b>作者：</b>kezhi<br>
 * <b>日期：</b> Thu Oct 20 22:01:07 CST 2016 <br>
 * <b>版权所有： 2015,dd5s.com<br>
 */ 

public interface IAreaDictService extends IBaseService<AreaDict>{

}
