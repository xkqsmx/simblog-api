package com.simblog.oaknt.service;

import java.util.List;

import com.simblog.oaknt.domain.User;

public interface IUserService extends IBaseService<User>{
	
	public List<User> check(String accout,String password);
	
	public boolean unique(String key, String info);
	
	User findUserByEmail(String email);
	
	void resetPassword(Long userId, String password);
	
	public User selectByUserName(String userName);
	
}
