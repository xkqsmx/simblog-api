package com.simblog.oaknt.service;

import com.simblog.oaknt.domain.User;
import com.simblog.oaknt.domain.VerificationRecord;
import com.simblog.oaknt.service.IBaseService;

public interface IVerificationRecordService extends IBaseService<VerificationRecord>{
	
	VerificationRecord findRecordByUuid(String uuid);

	String addRegisterRecord(User user);
	
	String addPasswordRecord(User user);
	
	boolean isAllowVerOfRegist(String email);

	boolean verificateRegister(VerificationRecord record);

	boolean isAllowVerOfPasswd(String email);

	boolean verificatePassword(VerificationRecord record);
	
	
}
