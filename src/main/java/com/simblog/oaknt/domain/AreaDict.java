package com.simblog.oaknt.domain;

import com.simblog.oaknt.domain.BaseDomain;

/**
 * 
 * <br>
 * <b>功能：</b>AreaDict<br>
 * <b>作者：</b>kezhi<br>
 * <b>日期：</b> Thu Oct 20 22:01:07 CST 2016 <br>
 * <b>版权所有： 2015,dd5s.com<br>
 */ 
@SuppressWarnings("serial")
public class AreaDict extends BaseDomain {
	
		private java.lang.Long id;//   	private java.lang.Long pid;//   	private java.lang.String name;//   名称	private java.lang.String code;//   	private java.lang.Integer sortIndex;//   索引值	private java.util.Date createTime;//   	private java.lang.String isHot;//   是否置顶   0否  1是	private java.lang.String isDelete;//   是否删除   0否   1是	public java.lang.Long getId() {	    return this.id;	}	public void setId(java.lang.Long id) {	    this.id=id;	}	public java.lang.Long getPid() {	    return this.pid;	}	public void setPid(java.lang.Long pid) {	    this.pid=pid;	}	public java.lang.String getName() {	    return this.name;	}	public void setName(java.lang.String name) {	    this.name=name;	}	public java.lang.String getCode() {	    return this.code;	}	public void setCode(java.lang.String code) {	    this.code=code;	}	public java.lang.Integer getSortIndex() {	    return this.sortIndex;	}	public void setSortIndex(java.lang.Integer sortIndex) {	    this.sortIndex=sortIndex;	}	public java.util.Date getCreateTime() {	    return this.createTime;	}	public void setCreateTime(java.util.Date createTime) {	    this.createTime=createTime;	}	public java.lang.String getIsHot() {	    return this.isHot;	}	public void setIsHot(java.lang.String isHot) {	    this.isHot=isHot;	}	public java.lang.String getIsDelete() {	    return this.isDelete;	}	public void setIsDelete(java.lang.String isDelete) {	    this.isDelete=isDelete;	}
}

