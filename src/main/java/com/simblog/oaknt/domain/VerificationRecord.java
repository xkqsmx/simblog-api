package com.simblog.oaknt.domain;

import com.simblog.oaknt.domain.BaseDomain;

@SuppressWarnings("serial")
public class VerificationRecord extends BaseDomain {
	private java.lang.Long id;//   
	private java.lang.Long userId;//   用户id
	private java.lang.String email;//   发送的邮箱
	private java.lang.String phone;//   发送的手机
	private java.lang.String type;//   类型 1注册认证  2找回密码
	private java.lang.String uuid;//   验证标识码
	private java.lang.String code;//   短信验证码
	private java.util.Date createTime;//   创建时间
	private java.util.Date lastValidTime;//   验证截止时间
	private java.util.Date verificateTime;//   验证通过时间
	private java.lang.String isVerificate;//   是否验证 0否 1是
	private java.lang.String isInvalid;//   是否失效 0否 1是
	private java.lang.String isDelete;//   是否删除  0否 1是
	public java.lang.Long getId() {
	    return this.id;
	}
	public void setId(java.lang.Long id) {
	    this.id=id;
	}
	public java.lang.Long getUserId() {
	    return this.userId;
	}
	public void setUserId(java.lang.Long userId) {
	    this.userId=userId;
	}
	public java.lang.String getEmail() {
	    return this.email;
	}
	public void setEmail(java.lang.String email) {
	    this.email=email;
	}
	public java.lang.String getPhone() {
	    return this.phone;
	}
	public void setPhone(java.lang.String phone) {
	    this.phone=phone;
	}
	public java.lang.String getType() {
	    return this.type;
	}
	public void setType(java.lang.String type) {
	    this.type=type;
	}
	public java.lang.String getUuid() {
	    return this.uuid;
	}
	public void setUuid(java.lang.String uuid) {
	    this.uuid=uuid;
	}
	public java.lang.String getCode() {
	    return this.code;
	}
	public void setCode(java.lang.String code) {
	    this.code=code;
	}
	public java.util.Date getCreateTime() {
	    return this.createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
	    this.createTime=createTime;
	}
	public java.util.Date getLastValidTime() {
	    return this.lastValidTime;
	}
	public void setLastValidTime(java.util.Date lastValidTime) {
	    this.lastValidTime=lastValidTime;
	}
	public java.util.Date getVerificateTime() {
	    return this.verificateTime;
	}
	public void setVerificateTime(java.util.Date verificateTime) {
	    this.verificateTime=verificateTime;
	}
	public java.lang.String getIsVerificate() {
	    return this.isVerificate;
	}
	public void setIsVerificate(java.lang.String isVerificate) {
	    this.isVerificate=isVerificate;
	}
	public java.lang.String getIsInvalid() {
	    return this.isInvalid;
	}
	public void setIsInvalid(java.lang.String isInvalid) {
	    this.isInvalid=isInvalid;
	}
	public java.lang.String getIsDelete() {
	    return this.isDelete;
	}
	public void setIsDelete(java.lang.String isDelete) {
	    this.isDelete=isDelete;
	}
}

