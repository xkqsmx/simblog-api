package com.simblog.oaknt.domain;

import com.simblog.oaknt.domain.BaseDomain;

@SuppressWarnings("serial")
public class Article extends BaseDomain {
	
		private java.lang.Long id;//   	private java.lang.Long userId;//   用户id	private java.lang.Long categoryId;//   类别id
	private java.lang.String categoryName;  //类别名称	private java.lang.String title;//   文章标题	private java.lang.String tags;//   文章标签，多个标签用‘,’隔开	private java.lang.String summary;//   文章概述	private java.lang.Object content;//   md版的文章正文	private java.lang.Object htmlContent;//   html版的文章正文	private java.util.Date createTime;//   发布时间	private java.util.Date updateTime;//   更新时间	private java.lang.String location;//   位置	private java.lang.String isPublic;//   是否公开   0否 1是
	private java.lang.String isTop;  //是否置顶   0否  1是	private java.lang.String isRecommend;//   是否推荐  0否   1是	private java.lang.String isLock;//   是否锁定   0否   1是	private java.lang.String isDelete;//   是否删除   0否  1是	public java.lang.Long getId() {	    return this.id;	}	public void setId(java.lang.Long id) {	    this.id=id;	}	public java.lang.Long getUserId() {	    return this.userId;	}	public void setUserId(java.lang.Long userId) {	    this.userId=userId;	}	public java.lang.Long getCategoryId() {	    return this.categoryId;	}	public void setCategoryId(java.lang.Long categoryId) {	    this.categoryId=categoryId;	}
	public java.lang.String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(java.lang.String categoryName) {
		this.categoryName = categoryName;
	}
	public java.lang.String getTitle() {	    return this.title;	}	public void setTitle(java.lang.String title) {	    this.title=title;	}	public java.lang.String getTags() {	    return this.tags;	}	public void setTags(java.lang.String tags) {	    this.tags=tags;	}	public java.lang.String getSummary() {	    return this.summary;	}	public void setSummary(java.lang.String summary) {	    this.summary=summary;	}	public java.lang.Object getContent() {	    return this.content;	}	public void setContent(java.lang.Object content) {	    this.content=content;	}	public java.lang.Object getHtmlContent() {	    return this.htmlContent;	}	public void setHtmlContent(java.lang.Object htmlContent) {	    this.htmlContent=htmlContent;	}	public java.util.Date getCreateTime() {	    return this.createTime;	}	public void setCreateTime(java.util.Date createTime) {	    this.createTime=createTime;	}	public java.util.Date getUpdateTime() {	    return this.updateTime;	}	public void setUpdateTime(java.util.Date updateTime) {	    this.updateTime=updateTime;	}	public java.lang.String getLocation() {	    return this.location;	}	public void setLocation(java.lang.String location) {	    this.location=location;	}	public java.lang.String getIsPublic() {	    return this.isPublic;	}	public void setIsPublic(java.lang.String isPublic) {	    this.isPublic=isPublic;	}
	public java.lang.String getIsTop() {
		return isTop;
	}
	public void setIsTop(java.lang.String isTop) {
		this.isTop = isTop;
	}
	public java.lang.String getIsRecommend() {	    return this.isRecommend;	}	public void setIsRecommend(java.lang.String isRecommend) {	    this.isRecommend=isRecommend;	}	public java.lang.String getIsLock() {	    return this.isLock;	}	public void setIsLock(java.lang.String isLock) {	    this.isLock=isLock;	}	public java.lang.String getIsDelete() {	    return this.isDelete;	}	public void setIsDelete(java.lang.String isDelete) {	    this.isDelete=isDelete;	}
}

