package com.simblog.oaknt.domain;

import com.simblog.oaknt.domain.BaseDomain;

@SuppressWarnings("serial")
public class User extends BaseDomain {
	
		private java.lang.Long id;//   ID
	private java.lang.Long uid;//   
	private java.lang.String account;//   账号
	private java.lang.String password;//   密码 md5加密
	private java.lang.String userName;//   
	private java.lang.String realName;//   真实姓名
	private java.lang.String nickName;//   昵称
	private java.lang.String phone;//   电话
	private java.lang.String email;//   邮箱
	private java.lang.String gender;//   性别 [ 男 ,女,中性]
	private java.util.Date birthday;//   
	private java.lang.String province;//   省份
	private java.lang.String city;//   城市
	private java.lang.String county;//   区/县
	private java.lang.String address;//   地址
	private java.lang.String school;//   毕业院校
	private java.lang.String major;//   专业
	private java.util.Date createTime;//   创建时间
	private java.util.Date updateTime;//   更新时间
	private java.lang.String isVerify;//   是否注册校验  0否   1是
	private java.lang.String isLock;//   是否锁定   0否  1是
	private java.lang.String isDelete;//   是否删除  0否   1是
	
	//extra 
	private String token;
	
	
	public java.lang.Long getId() {
	    return this.id;
	}
	public void setId(java.lang.Long id) {
	    this.id=id;
	}
	public java.lang.Long getUid() {
	    return this.uid;
	}
	public void setUid(java.lang.Long uid) {
	    this.uid=uid;
	}
	public java.lang.String getAccount() {
	    return this.account;
	}
	public void setAccount(java.lang.String account) {
	    this.account=account;
	}
	public java.lang.String getPassword() {
	    return this.password;
	}
	public void setPassword(java.lang.String password) {
	    this.password=password;
	}
	public java.lang.String getUserName() {
	    return this.userName;
	}
	public void setUserName(java.lang.String userName) {
	    this.userName=userName;
	}
	public java.lang.String getRealName() {
	    return this.realName;
	}
	public void setRealName(java.lang.String realName) {
	    this.realName=realName;
	}
	public java.lang.String getNickName() {
	    return this.nickName;
	}
	public void setNickName(java.lang.String nickName) {
	    this.nickName=nickName;
	}
	public java.lang.String getPhone() {
	    return this.phone;
	}
	public void setPhone(java.lang.String phone) {
	    this.phone=phone;
	}
	public java.lang.String getEmail() {
	    return this.email;
	}
	public void setEmail(java.lang.String email) {
	    this.email=email;
	}
	public java.lang.String getGender() {
	    return this.gender;
	}
	public void setGender(java.lang.String gender) {
	    this.gender=gender;
	}
	public java.util.Date getBirthday() {
	    return this.birthday;
	}
	public void setBirthday(java.util.Date birthday) {
	    this.birthday=birthday;
	}
	public java.lang.String getProvince() {
	    return this.province;
	}
	public void setProvince(java.lang.String province) {
	    this.province=province;
	}
	public java.lang.String getCity() {
	    return this.city;
	}
	public void setCity(java.lang.String city) {
	    this.city=city;
	}
	public java.lang.String getCounty() {
	    return this.county;
	}
	public void setCounty(java.lang.String county) {
	    this.county=county;
	}
	public java.lang.String getAddress() {
	    return this.address;
	}
	public void setAddress(java.lang.String address) {
	    this.address=address;
	}
	public java.lang.String getSchool() {
	    return this.school;
	}
	public void setSchool(java.lang.String school) {
	    this.school=school;
	}
	public java.lang.String getMajor() {
	    return this.major;
	}
	public void setMajor(java.lang.String major) {
	    this.major=major;
	}
	public java.util.Date getCreateTime() {
	    return this.createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
	    this.createTime=createTime;
	}
	public java.util.Date getUpdateTime() {
	    return this.updateTime;
	}
	public void setUpdateTime(java.util.Date updateTime) {
	    this.updateTime=updateTime;
	}
	public java.lang.String getIsVerify() {
	    return this.isVerify;
	}
	public void setIsVerify(java.lang.String isVerify) {
	    this.isVerify=isVerify;
	}
	public java.lang.String getIsLock() {
	    return this.isLock;
	}
	public void setIsLock(java.lang.String isLock) {
	    this.isLock=isLock;
	}
	public java.lang.String getIsDelete() {
	    return this.isDelete;
	}
	public void setIsDelete(java.lang.String isDelete) {
	    this.isDelete=isDelete;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}

