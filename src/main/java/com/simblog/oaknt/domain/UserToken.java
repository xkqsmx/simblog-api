package com.simblog.oaknt.domain;

import com.simblog.oaknt.domain.BaseDomain;

/**
 * 
 * <br>
 * <b>功能：</b>UserToken<br>
 * <b>作者：</b>kezhi<br>
 * <b>日期：</b> Mon Dec 19 20:49:17 CST 2016 <br>
 * <b>版权所有： 2015,dd5s.com<br>
 */ 
@SuppressWarnings("serial")
public class UserToken extends BaseDomain {
	
		private java.lang.Long id;//   	private java.lang.Long userId;//   	private java.lang.String token;//   	private java.util.Date createTime;//   	private java.util.Date updateTime;//   	private java.util.Date expiredTime;//   	private java.lang.String isDelete;//   	public java.lang.Long getId() {	    return this.id;	}	public void setId(java.lang.Long id) {	    this.id=id;	}	public java.lang.Long getUserId() {	    return this.userId;	}	public void setUserId(java.lang.Long userId) {	    this.userId=userId;	}	public java.lang.String getToken() {	    return this.token;	}	public void setToken(java.lang.String token) {	    this.token=token;	}	public java.util.Date getCreateTime() {	    return this.createTime;	}	public void setCreateTime(java.util.Date createTime) {	    this.createTime=createTime;	}	public java.util.Date getUpdateTime() {	    return this.updateTime;	}	public void setUpdateTime(java.util.Date updateTime) {	    this.updateTime=updateTime;	}	public java.util.Date getExpiredTime() {	    return this.expiredTime;	}	public void setExpiredTime(java.util.Date expiredTime) {	    this.expiredTime=expiredTime;	}	public java.lang.String getIsDelete() {	    return this.isDelete;	}	public void setIsDelete(java.lang.String isDelete) {	    this.isDelete=isDelete;	}
}

