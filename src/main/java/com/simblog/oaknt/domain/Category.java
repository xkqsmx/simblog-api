package com.simblog.oaknt.domain;

import com.simblog.oaknt.domain.BaseDomain;

@SuppressWarnings("serial")
public class Category extends BaseDomain {
	
		private java.lang.Long id;//   	private java.lang.Long userId;//   用户id	private java.lang.String name;//   类别名称	private java.lang.String content;//   类别说明	private java.lang.Integer orderIndex;//   排序位置	private java.util.Date createTime;//   创建时间	private java.lang.String isDefault;//   	private java.lang.String isPublic;//   是否公开  0否  1是	private java.lang.String isDelete;//   是否删除  0否   1是	public java.lang.Long getId() {	    return this.id;	}	public void setId(java.lang.Long id) {	    this.id=id;	}	public java.lang.Long getUserId() {	    return this.userId;	}	public void setUserId(java.lang.Long userId) {	    this.userId=userId;	}	public java.lang.String getName() {	    return this.name;	}	public void setName(java.lang.String name) {	    this.name=name;	}	public java.lang.String getContent() {	    return this.content;	}	public void setContent(java.lang.String content) {	    this.content=content;	}	public java.lang.Integer getOrderIndex() {	    return this.orderIndex;	}	public void setOrderIndex(java.lang.Integer orderIndex) {	    this.orderIndex=orderIndex;	}	public java.util.Date getCreateTime() {	    return this.createTime;	}	public void setCreateTime(java.util.Date createTime) {	    this.createTime=createTime;	}	public java.lang.String getIsDefault() {	    return this.isDefault;	}	public void setIsDefault(java.lang.String isDefault) {	    this.isDefault=isDefault;	}	public java.lang.String getIsPublic() {	    return this.isPublic;	}	public void setIsPublic(java.lang.String isPublic) {	    this.isPublic=isPublic;	}	public java.lang.String getIsDelete() {	    return this.isDelete;	}	public void setIsDelete(java.lang.String isDelete) {	    this.isDelete=isDelete;	}
}

